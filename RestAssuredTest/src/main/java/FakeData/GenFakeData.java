package FakeData;

import com.github.javafaker.Faker;

public class GenFakeData {
    public static void main(String[] args) {
        Faker faker=new Faker();

        System.out.println("First name : "+faker.name().firstName());
        System.out.println("Last Name : "+faker.name().lastName());
        System.out.println("City Name : "+faker.address().cityName());
        System.out.println("State Name : "+faker.address().state());
        System.out.println("Phone number : "+faker.phoneNumber().cellPhone());
        System.out.println("Card Type : "+faker.business().creditCardType());
        System.out.println("Card Number : "+faker.business().creditCardNumber());
        System.out.println("Card Expiry : "+faker.business().creditCardExpiry());
        System.out.println(" Book Author : "+faker.book().author());
        System.out.println("Book Publisher : "+faker.book().publisher());
        System.out.println("Book  Title : "+faker.book().title());

    }
}
