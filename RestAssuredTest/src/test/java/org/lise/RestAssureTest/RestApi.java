package org.lise.RestAssureTest;


import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.lise.BaseTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class RestApi extends BaseTest {


    @Test
    public void test(){

        Response response = given()
                .param("page","2")
//                .header("","")
                .accept(ContentType.JSON)
//                .auth().basic("","")
//                .body()
                .request(Method.GET,"/users");
        System.out.println("my res "+response.asString());
        JSONObject jsonObject = new JSONObject(response.asString());

        JSONArray array=jsonObject.getJSONArray("data");
        JSONObject posts = array.getJSONObject(0);
        System.out.println(posts.getString("email"));

        System.out.println(jsonObject.getInt("per_page"));
        System.out.println(jsonObject.getJSONArray("data"));
        JSONObject object = array.getJSONObject(0);
        assertThat(object.getInt("id"),equalTo(7));

        JSONObject support = jsonObject.getJSONObject("support");
        System.out.println("object "+support.toString());
//   assertThat(jsonObject.getString("data.id"),equalTo(7));
    }
@Test
    public void PostTest(){
        String data="{\n" +
                "    \"name\": \"ajay\",\n" +
                "    \"job\": \"leader\"\n" +
                "}\n";

        Response response= given()
                .accept(ContentType.JSON)
                .body(data)
                        .request(Method.POST,"/user");
        System.out.println("My Response "+response.statusCode());
    }
@Test
    public void DeleteTestApi(){
        Response response=given()
//                .param("2")
                .accept(ContentType.JSON)
                .request(Method.DELETE,"/users/2");
        System.out.println("my res "+response.statusCode());
    }
     @Test
    public void GetSingleUser(){
        Response response=given()
                .accept(ContentType.JSON)
                .request(Method.GET,"/user/2");
        System.out.println("SingleUser Res "+response.statusCode());
         System.out.println(response.asString());
         JSONObject jsonObject=new JSONObject(response.asString());
         JSONObject obj=jsonObject.getJSONObject("data");
         System.out.println(" single : "+obj);
         assertThat(obj.getInt("id"),is(2));

    }

}
