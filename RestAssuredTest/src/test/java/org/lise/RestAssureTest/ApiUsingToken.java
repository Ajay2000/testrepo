package org.lise.RestAssureTest;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import org.testng.annotations.Test;

public class ApiUsingToken {
    String url = "https://gorest.co.in/public/v2/users";

    @Test(priority = 1)
    public void CreateUserApi() {
        String apiCall = url;

//        Create payload
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", "john");
        jsonObject.put("age", 33);
        jsonObject.put("city", "india");
        jsonObject.put("email", "john323@gmail.com ");

        String payload = jsonObject.toString();

        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;

//        Call Api

        RequestSpecification requestSpecification = RestAssured.given()
                .header("Authorization", authToken).contentType("application/json").body(payload);




    }
}
