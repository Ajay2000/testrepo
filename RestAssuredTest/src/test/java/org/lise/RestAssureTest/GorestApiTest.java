package org.lise.RestAssureTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class GorestApiTest {
    @Test

    public void CreateUserApi() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;
        String data = "{\"name\":\"Denghali krishna\", \"gender\":\"female\", \"email\":\"tennali.jjfewfjh44400@15ce.com\", \"status\":\"active\"}";

        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .body(data)
                .request(Method.POST, "/users");
        System.out.println("My Response " + response.statusCode());
        System.out.println("Response " + response.asString());

        assertThat("Status code is not 201", response.statusCode(), is(HttpStatus.SC_CREATED));
    }

    @Test

    public void GetUserDetail() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;

        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .request(Method.GET, "/users/991322");
        System.out.println("My Response " + response.statusCode());
        System.out.println("User details : " + response.asString());

        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));

        JSONObject jsonObject = new JSONObject(response.asString());
        assertThat(jsonObject.getString("name"), equalTo("Denghali krishna"));
    }

    @Test

    public void UpdateUserDetail() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;
        String data = "{\n" +
                "    \"name\": \"mpheus\",\n" +
                "    \"gender\": \"male\"\n" +
                "}";
        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .body(data)
                .request(Method.PUT, "/users/991178");
        System.out.println("My Response " + response.statusCode());
        System.out.println("User details : " + response.asString());

        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));
    }

    @Test

    public void DeleteUserDetail() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;
        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .request(Method.DELETE, "/users/991322");
        System.out.println("My Response " + response.statusCode());

        assertThat("Status code is not 204", response.statusCode(), is(HttpStatus.SC_NO_CONTENT));
    }


    @Test

    public void GetUserDetailNotFound() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;

        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .request(Method.GET, "/users/991322");
        System.out.println("My Response " + response.statusCode());
        System.out.println("User details : " + response.asString());

        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));

        JSONObject jsonObject = new JSONObject(response.asString());
        assertThat(jsonObject.getString("name"), equalTo("Denghali krishna"));
    }





//    Nested++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    @Test
    public void NestedCreateUserApi() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;
        String data = "{  \n" +
                "    \"employee\": {  \n" +
                "        \"name\":       \"sonoo\",   \n" +
                "        \"salary\":      56000,   \n" +
                "        \"married\":    true  \n" +
                "    }  \n" +
                "}  \n";

        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .body(data)
                .request(Method.POST, "/users/991322");
        System.out.println("My Response " + response.statusCode());
        System.out.println("Response " + response.asString());

        assertThat("Status code is not 201", response.statusCode(), is(HttpStatus.SC_CREATED));
    }


    @Test
    public void NGetUserDetail() {
        RestAssured.baseURI = "https://gorest.co.in/public/v2";
        String token = "d2757b62378290dd86b7e4be40c8527318d506afdf3b3abd6a18d5765f2a8450";
        String authToken = "Bearer " + token;

        Response response = given()
                .header("Authorization", authToken)
                .contentType("application/json")
                .request(Method.GET, "/users/991322");
        System.out.println("My Response " + response.statusCode());
        System.out.println("User details : " + response.asString());

        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));

        JSONObject jsonObject = new JSONObject(response.asString());
        assertThat(jsonObject.getString("name"), equalTo("Denghali krishna"));
    }
}
