package org.lise.RestAssureTest;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class HamcrestTest {
    @Test
    public void GetApi() {
        int page, total;
//        int id;
        RestAssured.baseURI = "https://reqres.in/api";

        Response response = given().get("/user");
        page = JsonPath.from(response.getBody().asString()).get("page");
        total = JsonPath.from(response.getBody().asString()).get("total");

//        id= JsonPath.from(response.getBody().asString()).get("data");
//        Assertion
        assertThat((new Object[]{page, total}), is(new Object[]{1, 12}));
        assertThat((new Object[]{page}), equalTo(new Object[]{1}));

//        assertThat((new Object[]{id}),is(new Object[]{7}));
        System.out.println("My Response " + response.statusCode());
    }

    @Test
    public void GetSingleUser() {
        RestAssured.baseURI = "https://reqres.in/api";
        Response response = given()
                .accept(ContentType.JSON)
                .request(Method.GET, "/users/2");
        System.out.println("SingleUser Res " + response.statusCode());

        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));

        JSONObject jsonObject = new JSONObject(response.asString());
        JSONObject data = jsonObject.getJSONObject("data");
        assertThat("", data.getInt("id"), equalTo(2));
    }

    @Test
    public void GetDataNotFound() {
        RestAssured.baseURI = "https://reqres.in/api";
        Response response = given()
                .accept(ContentType.JSON)
                .request(Method.GET, "/users/23");
        System.out.println("Data not found Res " + response.statusCode());
    }

    @Test
    public void GetResource() {
        RestAssured.baseURI = "https://reqres.in/api/";
        Response response = given()
                .accept(ContentType.JSON)
                .request(Method.GET, "/unknown");
        System.out.println("Data Res " + response.statusCode());

        JSONObject jsonObject = new JSONObject(response.asString());
        JSONArray array = jsonObject.getJSONArray("data");

        JSONObject object = array.getJSONObject(0);
        assertThat(object.getInt("id"), equalTo(1));

        JSONObject support = jsonObject.getJSONObject("support");
        System.out.println("object " + support.toString());


    }

    @Test
    public void PostRegistration() {
        Faker faker = new Faker();
        String name = faker.internet().emailAddress();
        String data = "{\"email\": \"" + name + "\",\"password\": \"pistol\"}";
        RestAssured.baseURI = "https://reqres.in/api/register";
        Response response = given()
                .accept(ContentType.JSON)
                .body(data)
                .put("/register");
        System.out.println("Registration successfull - " + response.statusCode());

    }

    @Test
    public void UnsuccessFullRegister() {
        String data = "{\n" +
                "    \"email\": \"sydney@fife\"\n" +
                "}\n";
        RestAssured.baseURI = "https://reqres.in/api";
        Response response = given()
                .accept(ContentType.JSON)
                .body(data)
                .request(Method.POST, "/register");
        System.out.println("Registration Unsuccessfull : " + response.statusCode());
    }

    @Test
    public void GetDelay() {
        RestAssured.baseURI = "https://reqres.in/api/";
        Response response = given()
                .param("delay", "3")
//                .header("","")
                .accept(ContentType.JSON)
//                .auth().basic("","")
//                .body()
                .request(Method.GET, "/users");
        System.out.println("my res " + response.statusCode());
        JSONObject jsonObject = new JSONObject(response.asString());

        JSONArray array = jsonObject.getJSONArray("data");
        JSONObject posts = array.getJSONObject(0);
        System.out.println(posts.getString("email"));

        System.out.println(jsonObject.getInt("per_page"));
        System.out.println(jsonObject.getJSONArray("data"));
        JSONObject object = array.getJSONObject(0);
        assertThat(object.getString("first_name"), equalTo("George"));

        JSONObject support = jsonObject.getJSONObject("support");
        System.out.println("object " + support.toString());
    }


//    Create post using hashmap
    @Test
    public void TestUsingHashMap() {
        HashMap data = new HashMap();
        data.put("name", "scott");
        data.put("location", "finance");
        data.put("phone", "34124123");

        String courseAll[] = {"c", "c++", "java"};
        data.put("courses", courseAll);

        RestAssured.baseURI = "https://reqres.in/api/register";
        Response response = given()
                .accept(ContentType.JSON)
                .body(data)
                .put("/register");
        System.out.println("Registration successfully - " + response.statusCode());
        assertThat("Status code is not 200", response.statusCode(), is(HttpStatus.SC_OK));
    }

}
