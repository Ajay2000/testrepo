package org.lise;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class BaseTest {
    @BeforeSuite
    public void beforeTest(){
        RestAssured.baseURI = "https://reqres.in/api";

    }

}
